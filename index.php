<?php
    include('config.php');

    $page = !empty($_GET['page']) ? $_GET['page'] : null;
    if (empty($page) && !empty($_POST['page'])) {
        $page = $_POST['page'];
    }

    if (isset($_SESSION['logged_user'])) { 

        if (!empty($_GET['is_ajax'])) {
            define('AJAX_REQUEST', true);
        }
        
        if (!defined('AJAX_REQUEST')) {
            $smarty->assign('user', $_SESSION['logged_user']);
            $smarty->display('smarty/templates/header.tpl');
        }            

        if (!isset($page)) {
            include('controllers/main.php');           
            
        } elseif ($page == 'shop') {
            include('controllers/shop.php');   
    
        } elseif ($page == 'product') {
            include('controllers/product.php');

        } elseif ($page == 'profile') {
            include('controllers/profile.php');

        } elseif ($page == 'cart') {
            include('controllers/cart.php');

        } elseif ($page == 'order') {
            include('controllers/order.php');
        } elseif ($page == 'orderComplete') {
            include('controllers/order-complete.php');
        } 
      
        if (!defined('AJAX_REQUEST')) {
            $smarty->display('smarty/templates/footer.tpl');
        }

    } elseif($page == 'signup'){
        include('controllers/signup.php');
    } else {
        include('controllers/login.php');
    } 
    

          
?>

