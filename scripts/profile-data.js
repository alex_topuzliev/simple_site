// данные для входа
$(function() {
    $('#dataChangeForm').submit(function(e) {
      var $form = $(this);
      $.ajax({ 
        data: $form.serialize()
      }).done(function() {
        alert('Данные изменения сохранены!');
      }).fail(function() {
        console.log('fail');
      });
      e.preventDefault(); 
    });
});

// Одинаковые адреса доставки и оплаты
$("input[name=same_address]").on("change", function () {
  var country = $("input[name=country]").val();
  var city = $("input[name=city]").val();
  var address = $("input[name=address]").val();

  $("input[name=country_billing]").val(country);
  $("input[name=city_billing]").val(city);
  $("input[name=address_billing]").val(address);
});

// shippingAddress
$(function() {
  $('#shippingAddress').submit(function(e) {
    var $form = $(this);
    $.ajax({
      data: $form.serialize()
    }).done(function() {
      alert('Данные изменения сохранены!');
    }).fail(function() {
      console.log('fail');
    });
    e.preventDefault(); 
  });
});

// billingAddress
$(function() {
  $('#billingAddress').submit(function(e) {
    var $form = $(this);
    $.ajax({
      data: $form.serialize()
    }).done(function() {
      alert('Данные изменения сохранены!');
    }).fail(function() {
      console.log('fail');
    });
    e.preventDefault(); 
  });
});