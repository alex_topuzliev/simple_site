$("#pagination li a").on('click',function(){
 
	var page = $(this).data('pageId');
	var page_num = $(this).data('pageNum');
	var limit = $(this).data('limit');
	if(isNaN(limit)){
		limit=3;
	}
			
	$.ajax({
		method: "GET",
		url: "index.php",
		data: {
			limit:limit,
			page_num: page_num,
			page: page,
			is_ajax: 1
		}		
	}).done(function( data ) {
		$("#content").replaceWith(data); 		
	});
});
	
$("select[name=limit]").on("change", function () {					
	var limit = $(this).val();
	var page = $(this).data('pageId');
	
	$.ajax({
		method: "GET",
		url: "index.php",
		data: {
			limit: limit,
			page: page,
			is_ajax: 1
		}		
	}).done(function( data ) {
		$("#content").replaceWith(data); 		
	});
								
}); 
