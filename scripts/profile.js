// profile-menu

$("#profile-menu a").on('click',function(){ 
  var block = $(this).data('pageBlock');
  var page = $(this).data('pageId');

  $.ajax({
		method: "GET",
		url: "index.php",
		data: {
      block:block,
			page: page,
			is_ajax: 1
		}		
	}).done(function(data) {
    $("#content").replaceWith(data);	
	});

}); 

// удаление из избранного
$(".deleteFavorites").on('click',function(){ 
  var good_id = $(this).data('good_id');
  var page = $(this).data('pageId');
  var block = 'favorites';
  var page = 'profile';
  $.ajax({   
      type:'GET',
      data: {
        good_id:good_id,
        page: page,
      },
      url: "index.php"
      
  }).done(function() {
      alert('Товар удален из избранного'); 	
      $.ajax({
        method: "GET",
        url: "index.php",
        data: {
          block:block,
          page: page,
          is_ajax: 1
        }		
      }).done(function(data) {
        $("#content").replaceWith(data);	
      });
  });
}); 


