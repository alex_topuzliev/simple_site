// Одинаковые адреса
$("input[name=same_address]").on("change", function () {
  var country = $("input[name=country]").val();
  var city = $("input[name=city]").val();
  var address = $("input[name=address]").val();

  $("input[name=country_billing]").val(country);
  $("input[name=city_billing]").val(city);
  $("input[name=address_billing]").val(address);
});

// Отправка заявки
$('#addressForm').submit(function(e) {
  e.preventDefault();
  $.post('index.php', $('#addressForm').serialize(), onAjaxSuccess);
  $.ajax({   
    type:'GET',
    data: {
      page: 'orderComplete',
      is_ajax: 1
    },
    url: "index.php?"
    }).done(function(data) {
      $('#content').replaceWith(data); 		
  });
});

function onAjaxSuccess () {
  alert ('Заявка успешно отправлена');
} 

