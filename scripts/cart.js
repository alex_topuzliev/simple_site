// Удаление товара из корзины
$(".exclude").on('click',function(){ 
    var good_id = $(this).data('good_id');
    var page = $(this).data('pageId');
    $.ajax({   
        type:'GET',
        data: {
          good_id:good_id,
          page: page,
          exclude: 'exclude',
          is_ajax: 1
        },
        url: "index.php"
    }).done(function(data) {
        alert('Товар удален из корзины'); 
        location.reload();
        //$("#content").load('index.php?page=cart' + ' #content');
	});
});

// Кол-во товара
$(document).ready(function() {
    var total=0;
    $('.table-cart tbody tr').each(function(){
        total+=parseInt($(this).find('.count_price').text());
    });
    $('.total_price').text(total);

    function change($tr, val) {
        var $input = $tr.find('.quantity');
        var count = parseInt($input.val()) + val;
        count = count < 1 ? 1 : count;
        $input.val(count);
        var $price = $tr.find('.count_price');
        $price.text(count * $price.data('price'));
        
        var this_good = $tr.find('.exclude');
        var good_id = this_good.data('good_id');
        $.ajax({   
            type:'GET',
            data: {
                count: count,
                good_id:good_id
            },
            url: "index.php?page=cart"    
        })
       
        var total=0;
        $('.table-cart tbody tr').each(function(){
            total+=parseInt($(this).find('.count_price').text());
        });
        $('.total_price').text(total);
    }

    $('.minus').click(function() {
        change($(this).closest('tr'), -1);
    });
    $('.plus').click(function() {
        change($(this).closest('tr'), 1);
    });
    $('.quantity').on("input", function() {
        var $price = $(this).closest('tr').find('.count_price');
        $price.text(this.value * $price.data('price'));
    });
});

// Оформит заказ
$("#do_order").on('click',function(){ 
    var page = 'order';
    var total_price = $('.total_price').html();
    $.ajax({   
        type:'GET',
        data: {
            page: page,
            total_price,
            is_ajax: 1
        },
        url: "index.php?"
        
    }).done(function(data) {
        alert('Перейти к оформлению заявки?')
		$('#content').replaceWith(data); 		
	});
}); 

