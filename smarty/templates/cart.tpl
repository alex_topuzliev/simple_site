<div id="content">
    <div class="cart">
        <table class="table-cart">
            <thead>
                <tr>
                    <th>Фото</th>
                    <th>Название</th>
                    <th>Цена</th>
                    <th>Количество</th>
                    <th>Сумма</th>
                    <th></th>
                </tr>
            </thead>           
            <tbody>
                {foreach from=$cart item=good}
                    <tr>
                        <td><img src="{$good['img']}"></td>
                        <td>{$good['name']}</td>
                        <td>{$good['price']} <span> &#8381;</span></td>
                        <td>
                            <div class="quantity-box wrapper-row">
                                <button class="minus">-</button>
                                <input class="quantity" type="text" value="1" size="1"/>
                                <button class="plus">+</button>
                            </div>
                        </td>
                        <td>
                            <span class="count_price" data-price="{$good['price']}">{$good['price']}</span> &#8381;
                        </td>
                        <td>
                            <button class="exclude" data-good_id="{$good['good_id']}" data-page-id="{$smarty.request.page}">
                                <img src="images/delete-icn.svg" alt="Удалить из корзины"> </button>
                            </button>
                        </td>  
                    </tr>
                {{/foreach}}
           
            </tbody>    
        </table>

            <div class="total">
                Итого: <span class="total_price">Сумма итого</span> руб.
            </div> 
            <div class="btn" >
                <a id="do_order">Оформить заказ</a>
            </div> 
          
    </div>

</div>



<script type="text/javascript" src="scripts/cart.js"></script>