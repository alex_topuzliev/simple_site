<table class="table-cart">
    <thead>
        <tr>
            <th>№ заказа</th>
            <th>Время создания заказа</th>
            <th>Стоимость</th>
            <th>Адреса</th>
        </tr>
    </thead>           
    <tbody>
        {foreach from=$orders item=order}
            <tr>
                <td>{$order['order_id']}</td>
                <td>{$order['order_time']}</td>
                <td>{$order['total_price']} &#8381;</td>
                <td>{$order['address']}</td>
            </tr>
        {/foreach}        
    </tbody>  
</table>