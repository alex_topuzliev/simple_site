<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet/less" type="text/css" href="styles/style.less">
        <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>  
    </head>
    <body>
        
        <form action="" method="POST" class="ui-form">
            <h3>Войти на сайт</h3>
            <div class="form-row">
                <input type="text" name="email" id="email" required>
                <label for="email">Email</label>
            </div>
            <div class="form-row">
                <input type="password" name="password" id="password" required>
                <label for="password">Пароль</label> 
                <div id="error-message"></div>
            </div>
            <p>
                <button type="submit" name="do_login">Войти</button>
            </p>
            <div class="link">
                <a href="index.php?page=signup">У вас нет аккаунта? Регистрация</a>
            </div>           
        </form> 
    
    </body>
    
</html>
