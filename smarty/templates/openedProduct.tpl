<div id="content"> 
    <div id="openedProduct">
        <div id="openedProduct-img">
            <img src="{$good['img']}">
        </div>
        <div id="openedProduct-content">
            <h1 id="openedProduct-name">
                {$good['name']}
            </h1>
            <div id="openedProduct-desc">
                {$good['description']}
            </div>

            <div class="actions">
                <div class="shopUnitPrice">
                    <div>Цена: {$good['price']} <span> &#8381;</span></div>
                </div>
                <div class="add-to-cart">
                    <a data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}' class="cart-button">В корзину</a>
                </div>
                <div class="add-to-links">
                    <a data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}' class="wishlist"></a>
                </div>
            </div>
            <div id="openedProduct-backlink">
                <a href="index.php?page=shop">НАЗАД</a>
            </div>

        </div>    
    </div>
</div>

<script type="text/javascript" src="scripts/in-cart.js"></script>