<div id="content">
    <h2>Ваш заказ размещен! </h2>
    <table class="table-cart">
        <thead>
            <tr>
                <th>Название</th>
                <th>Цена</th>
                <th>Количество</th>
            </tr>
        </thead>           
        <tbody>
            {foreach from=$order_details item=good}
                <tr>
                    <td>{$good['good_name']}</td>
                    <td>{$good['good_price']} <span>&#8381;</span></td>
                    <td>{$good['count']} </td>
                </tr>
            {/foreach}        
        </tbody>  
    </table>

    <div class="total">
        Общая сумма: <span class="total_price">{$total_price}</span> руб.
    </div> 

    <div class="order-address">
        {$address}
    </div>
</div>