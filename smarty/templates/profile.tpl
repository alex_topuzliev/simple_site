<div id="content">
    <div class="wrapper-row profile-menu" id="profile-menu"> 
        <a data-page-id='{$smarty.request.page}' data-page-block='info' class="{if $block eq 'info'}activeBlock{/if} "><h3>Информация профиля</h3></a>
        <a data-page-id='{$smarty.request.page}' data-page-block='favorites' class="{if $block eq 'favorites'}activeBlock{/if}"><h3>Избранное</h3></a>
        <a data-page-id='{$smarty.request.page}' data-page-block='orders' class="{if $block eq 'orders'}activeBlock{/if}"><h3>Заказы</h3></a>   
    </div>
    {if $block eq 'info'}
        {include file='profileInfo.tpl'}
    {/if}
    {if $block eq 'favorites'}
        {include file='favorites.tpl'}
    {/if}
    {if $block eq 'orders'}
        {include file='orders.tpl'}
    {/if}
</div>

<script type="text/javascript" src="scripts/profile.js"></script>
<script type="text/javascript" src="scripts/profile-data.js"></script>




