<div class="wrapper-column">
{foreach from=$favorites item=good}
    <div class="wrapper-row favorites-box">
        <div class="favorites">
            <img src="{$good['img']}"/>
        </div>
        <div class="favorites favorites-name">
            {$good['name']}
        </div>
        <div class="favorites">
            {$good['description']}
        </div>
        <div class="favorites favorites-price">
            {$good['price']} <span> &#8381; </span>
        </div>
        <button class="deleteFavorites" data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}'> 
            <img src="images/delete-icn.svg" alt="Удалить из корзины"> 
        </button>
        <div class="add-to-cart">
            <a data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}' class="cart-button"></a>
        </div>
        
    </div>
{{/foreach}}
</div>

<script type="text/javascript" src="scripts/in-cart.js"></script>