<div class="wrapper-row"> 
    <div class="profile-box">  
        <h3>Адрес доставки:</h3>
        <div class="wrapper-row">
            <div class="profile-data wrapper-column">
                <label>Страна:</label>
                <label>Город:</label>
                <label>Адрес:</label>   
            </div>
            <div class="profile-data ">
                <form id="shippingAddress">
                    <input type="text" name="country" value="{$shippingData.country}">
                    <input type="text" name="city" value="{$shippingData.city}">
                    <input type="text" name="address" value="{$shippingData.address}">
                    <p><button type="submit">cохранить</button></p>
                </form>
            </div>
        </div>
                    
        <div class="wrapper-row address-check">
            <p>Адрес доставки и платежа совпадают?</p>
            <input type="checkbox" name="same_address">
        </div>
    </div>      
    <div class="profile-box"> 
        <h3>Адрес платежа:</h3>
        <div class="wrapper-row">
            <div class="profile-data wrapper-column">
                <label>Страна:</label>
                <label>Город:</label>
                <label>Адрес:</label>   
            </div>
            <div class="profile-data">
                <form id="billingAddress">
                    <input type="text" name="country_billing" value="{$billingData.country}">
                    <input type="text" name="city_billing" value="{$billingData.city}">
                    <input type="text" name="address_billing" value="{$billingData.address}">
                    <p><button type="submit">cохранить</button></p>
                </form>
            </div>
        </div>               
    </div>
            
    <div class="profile-box">
        <h3>Данные для входа</h3>
        <div class="wrapper-row">
            <div class="profile-data wrapper-column">
                <label>Почта:</label>
                <label>Пароль:</label>
                <label>Имя:</label>
                <label>Фамилия:</label>    
            </div>
            <div class="profile-data">
                <form id="dataChangeForm">
                    <input type="text" name="email" value="{$oldData.email}" required>
                    <input type="password" name="password" placeholder="введите новый пароль" required>
                    <input type="text" name="name" value="{$oldData.name}" required>
                    <input type="text" name="lastname" value="{$oldData.lastname}" required>
                    <p><button type="submit" >cохранить</button></p>
                </form>
            </div>
        </div>                
    </div>
</div>   