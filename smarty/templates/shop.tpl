<div id="content">   		
    <div id="target-content">
        {foreach from=$goods item=good}
            <div class="shopUnit">
                <div class="shopUnitImg">
                    <img src="{$good['img']}"/>
                </div>
                <div class="shopUnitName">
                    {$good['name']}
                </div>
                <div class="shopUnitShortDesc">
                    {$good['description']}
                </div>

                <div class="actions">
                    <div class="shopUnitPrice">
                        <div>Цена: {$good['price']} <span> &#8381;</span></div>
                    </div>
                    <div class="add-to-cart">
                        <a data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}' class="cart-button">В корзину</a>
                    </div>
                    <div class="add-to-links">
                        <a data-good_id='{$good['good_id']}' data-page-id='{$smarty.request.page}' class="wishlist"></a>
                    </div>
                </div>
            
                <a href="index.php?page=product&id={$good['good_id']}" class="shopUnitMore">
                    Подробнее
                </a>
            </div>
        {/foreach}
    </div>

    <div class='paginationWrap'>
        <ul class='pagination' id="pagination">
            {section name=for loop=$total_pages}                     
                {assign var="i" value="{$smarty.section.for.iteration}"}
                <li class='{if $i == $smarty.request.page_num || !$smarty.request.page_num && $i == 1}active{/if}'  id="{$i}" >
                    <a data-page-id='{$smarty.request.page}' data-page-num='{$i}' data-limit='{$smarty.request.limit}'>{$i}</a>
                </li>            
            {/section}
        </ul>
        <span> Кол-во товаров: </span>       
        <select name="limit" data-page-id="{$smarty.request.page}">    
            <option>{$limit}</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="6">6</option>                   
        </select>                                                                     
    </div>

    <script type="text/javascript" src="scripts/pagination.js"></script>
    <script type="text/javascript" src="scripts/in-cart.js"></script>
    
</div>

