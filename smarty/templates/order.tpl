<div id="content">
    <div class="adress-box">
        <form method="POST" action="index.php" id="addressForm">
            <input type="hidden" name="page" value="order">
            <h3>Адрес доставки:</h3>
            <div class="profile-data wrapper-row">
                <label>Страна:</label>
                <input type="text" name="country" value="{$shippingData.country}">
                <label>Город:</label>
                <input type="text" name="city" value="{$shippingData.city}">
                <label>Адрес:</label>  
                <input type="text" name="address" value="{$shippingData.address}"> 
            </div>
    
            <div class="wrapper-row address-check">
                <p>Адрес доставки и платежа совпадают?</p>
                <input type="checkbox" name="same_address">
            </div>

            <h3>Адрес платежа:</h3>
            <div class="profile-data wrapper-row">
                <label>Страна:</label>
                <input type="text" name="country_billing" value="{$billingData.country}">
                <label>Город:</label>
                <input type="text" name="city_billing" value="{$billingData.city}">
                <label>Адрес:</label> 
                <input type="text" name="address_billing" value="{$billingData.address}">  
            </div> 
            <p><button class="but" id="finish_order" type="submit"> Отправить заявку </button></p>
            <input style="display:none" type="text" name="total_price" value="{$total_price}">  
        </form>    
    </div>
</div>       

<script type="text/javascript" src="scripts/order.js"></script>