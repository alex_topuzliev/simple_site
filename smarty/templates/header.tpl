<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    <link rel="stylesheet/less" type="text/css" href="styles/style.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
    <title>Онлайн магазин</title>
</head>
<body>
<header>
    <div id="headerInside">
        <div id="logo"></div>
        <div id="companyName">Brand</div>
        <div id="navWrap">
            <a href="index.php">
                Главная
            </a>
            <a href="index.php?page=shop">
                Магазин
            </a>
            <a href="index.php?page=profile">
                <i class="fa fa-user"></i><b> {$user.name}</b>   
            </a>
            <a href="index.php?page=cart">
                <img class="navWrap-img" src="images/cart-60-64.png"> 
            </a>
            <a href="controllers/logout.php">
                <img class="navWrap-img" src="images/logout-64.png">               
            </a>   
        </div>

        <div class="nav-toggle"><span></span></div>
        <ul id="menu">
                <li><a href="index.php">ГЛАВНАЯ</a></li>
                <li><a href="index.php?page=shop">МАГАЗИН</a></li>
        </ul>
    </div>
</header>
<body>