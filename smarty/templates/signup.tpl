<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet/less" type="text/css" href="styles/style.less">
        <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>                       
    </head>
    <body>
        
        <form name='registration' action="" method="POST" class="ui-form">
            <h3>Регистрация</h3>
            <div class="form-row">
                <input type="text" name="email" id="email" required>
                <label for="email">Email</label>
            </div>
            <div class="form-row">
                <input type="password" name="password" id="password"  required>
                <label for="password">Пароль</label> 
                <div id="error-message"></div>
            </div>

            <div class="form-row">
                <input type="text" name="name" id="name" required>
                <label for="name">Имя</label> 
            </div>
            <div class="form-row">
                <input type="text" name="lastname" id="lastname" required>
                <label for="lastname">Фамилия</label>
            </div>
            <p>
                <button type="submit" name="do_signup">Зарегистрироваться</button>
            </p>
            <div class="link">
                <a href="index.php">На главную</a>
            </div>           
        </form>

    </body>
    
</html>
