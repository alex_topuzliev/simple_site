<div align="center">Информация о заказе:</div><br>
<div>Информация о клиенте : {$name} {$lastname}.</div><br>
<div>{$address}</div><br>

<table>
    <thead>
        <tr>
            <th>Название</th>
            <th>Цена</th>
            <th>Количество</th>
        </tr>
    </thead>            			 
    <tbody>
        {foreach from=$order_details item=good}
            <tr>
                <td>{$good['good_name']}</td>
                <td>{$good['good_price']} <span>руб.</span></td>
                <td align="center">{$good['count']} </td>
            </tr>
        {/foreach}        
    </tbody> 
</table><br>

<div align="center"> Общая сумма: {$total_price} &#8381; </div><br>
<div align="center"> Спасибо за заказ! </div>'