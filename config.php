<?php

// Smarty
define('SMARTY_DIR', 'smarty/libs/');
require(SMARTY_DIR . 'Smarty.class.php');
$smarty = new Smarty();

$smarty->template_dir = 'smarty/templates/';
$smarty->compile_dir = 'smarty/templates_c/';
$smarty->config_dir = 'smarty/configs/';
$smarty->cache_dir = 'smarty/cache/';
 
// DB
$connection = mysqli_connect('localhost', 'root', '123', 'task');
mysqli_set_charset($connection,"utf8");

session_start ();
?>
