-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 06 2019 г., 18:36
-- Версия сервера: 5.7.27-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `task`
--

-- --------------------------------------------------------

--
-- Структура таблицы `address`
--

CREATE TABLE `address` (
  `address_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_type` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `address_type`, `country`, `city`, `address`) VALUES
(22, 56, 'shipping', 'Россия', 'Ростов', 'ул. Фрунзе, 2в, 35'),
(23, 52, 'shipping', 'Россия', 'Самара', 'пр-т Ленина 6,6'),
(24, 57, 'shipping', 'Россия', 'Казань', 'ул.Ленина, 198, 111'),
(25, 56, 'billing', 'Россия', 'Ростов', 'ул. Фрунзе, 2в, 35'),
(26, 57, 'billing', 'Россия', 'Ульяновск', 'ул. Сурова,3'),
(27, 52, 'billing', 'Россия', 'Самара', 'пр-т Ленина 6,6'),
(28, 67, 'shipping', 'Россия', 'Ульяновск', 'ул.Ленина, 198, 17'),
(29, 67, 'billing', 'Россия', 'Ульяновск', 'ул.Ленина, 198, 17');

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `good_id` int(11) NOT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `good_id`, `count`) VALUES
(154, 56, 3, 1),
(156, 56, 12, 1),
(158, 56, 5, 1),
(159, 56, 2, 1),
(201, 67, 2, 1),
(202, 67, 11, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `favorites`
--

CREATE TABLE `favorites` (
  `favorites_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `good_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `favorites`
--

INSERT INTO `favorites` (`favorites_id`, `user_id`, `good_id`) VALUES
(13, 57, 11),
(14, 57, 9),
(17, 57, 6),
(18, 57, 1),
(29, 56, 8),
(30, 56, 9),
(31, 66, 2),
(32, 66, 3),
(33, 66, 1),
(40, 56, 1),
(41, 56, 2),
(42, 56, 11),
(43, 56, 12),
(44, 56, 10),
(45, 56, 5),
(46, 56, 3),
(54, 67, 2),
(55, 67, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE `goods` (
  `good_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `description` varchar(3072) DEFAULT NULL,
  `img` varchar(191) DEFAULT NULL,
  `price` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`good_id`, `name`, `description`, `img`, `price`) VALUES
(1, 'Зеркальный фотоаппарат Fujifilm X-T30 Body', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Fujifilm X-T30 Body.jpg', '59990'),
(2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Olympus Tough TG-6.jpg', '33990'),
(3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Canon EOS 6D Mark II Body.jpg', '87990'),
(4, 'Фотоаппарат Sony Alpha A6000 kit 16-50 f/3.5-5.6 OSS', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Sony Alpha A6000 kit.jpg', '34990'),
(5, 'Моментальная фотокамера Kodak Mini Shot', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Kodak Mini Shot.jpg', '5490'),
(6, 'Цифровой фотоаппарат Nikon Coolpix W300', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Nikon Coolpix W300.jpg', '87990'),
(7, 'Моментальная фотокамера Polaroid Snap Touch', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Polaroid Snap Touch.jpg', '13990'),
(8, 'Зеркальный фотоаппарат Canon EOS 5D Mark IV Kit EF 24-70mm f/4L', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Canon EOS 5D Mark IV Kit EF.jpg', '199990'),
(9, 'Фотоаппарат Olympus PEN E-PL9 kit 14-42mm f/3.5-5.6 EZ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Olympus PEN E-PL9 kit 14-42mm.jpg', '58900'),
(10, 'Фотоаппарат Sigma fp', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Sigma fp.jpg', '99990'),
(11, 'Цифровой фотоаппарат Sony Cyber-shot DSC-RX100M6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Sony Cyber-shot DSC-RX100M6.jpg', '70990'),
(12, 'Panasonic Lumix DMC-FZ300EEK', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium', 'images/goods/Panasonic Lumix DMC-FZ300EEK.jpg', '34490');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_time` varchar(191) DEFAULT NULL,
  `total_price` int(11) NOT NULL,
  `address` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_time`, `total_price`, `address`) VALUES
(206, 67, '05.11.2019 16:28:17', 121980, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(215, 67, '06.11.2019 13:06:15', 181970, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(223, 67, '06.11.2019 15:15:52', 93980, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(224, 67, '06.11.2019 15:30:12', 121980, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(225, 67, '06.11.2019 15:31:50', 1233930, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(226, 67, '06.11.2019 15:37:07', 121980, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(227, 67, '06.11.2019 15:40:41', 473940, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17'),
(228, 67, '06.11.2019 16:26:49', 221970, 'Адрес доставки: Россия, Ульяновск, ул.Ленина, 198, 17 ; Адрес платежа: Россия, Ульяновск, ул.Ленина, 198, 17');

-- --------------------------------------------------------

--
-- Структура таблицы `order_details`
--

CREATE TABLE `order_details` (
  `order_id` int(11) NOT NULL,
  `good_id` int(11) NOT NULL,
  `good_name` varchar(191) DEFAULT NULL,
  `good_price` varchar(191) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_details`
--

INSERT INTO `order_details` (`order_id`, `good_id`, `good_name`, `good_price`, `count`) VALUES
(215, 1, 'Зеркальный фотоаппарат Fujifilm X-T30 Body', '59990', 1),
(215, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(215, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(216, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(216, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(217, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(217, 1, 'Зеркальный фотоаппарат Fujifilm X-T30 Body', '59990', 1),
(219, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(219, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(220, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(220, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(223, 1, 'Зеркальный фотоаппарат Fujifilm X-T30 Body', '59990', 1),
(223, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(224, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(224, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(225, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(225, 8, 'Зеркальный фотоаппарат Canon EOS 5D Mark IV Kit EF 24-70mm f/4L', '199990', 6),
(226, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(226, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(227, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(227, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 5),
(228, 2, 'Цифровой фотоаппарат Olympus Tough TG-6\r\n', '33990', 1),
(228, 3, 'Зеркальный фотоаппарат Canon EOS 6D Mark II Body', '87990', 1),
(228, 10, 'Фотоаппарат Sigma fp', '99990', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `lastname`, `status`) VALUES
(1, 'admin@mail.ru', '$2y$10$enJz1mf0.wyan76BuEf7Oupdm8etxS40gn/.tlZcIwSLjW/rIEoe.', 'admin', 'admin', 'admin'),
(56, '1234', '$2y$10$IKBkoy/qpbOPNDIFjPHvR./XwmTbjAow2uPoFOBLJYMnTGomGYwSy', '1234', '123аааа', 'user'),
(57, 'tester@mail.ru', '$2y$10$bSbiDh/x9eFK/CdpuEHL3egzvKL06rFJu7TegkJvYBi2WTlY64g/G', 'tester', 'tester2', 'user'),
(58, 'name@mail.ru', '$2y$10$8q2aUIN58VFQ8WkNwHHtk.iay7NJ6q.rRiosV.NNrJ2KMELgkodU.', 'name', 'name', 'user'),
(67, 'alexander_789@mail.ru', '$2y$10$WN81hpnPCupqAd0KYL8.Z.CU9xW3d.fQ32Qk57jfT43YUihSfiL3y', 'Alex', 'T', 'user');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `users_id` (`user_id`),
  ADD KEY `users_id_2` (`user_id`);

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `good_id` (`good_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`favorites_id`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`good_id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `order_details`
--
ALTER TABLE `order_details`
  ADD KEY `good_id` (`good_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT для таблицы `favorites`
--
ALTER TABLE `favorites`
  MODIFY `favorites_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `good_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
